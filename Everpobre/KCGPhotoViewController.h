//
//  KCGPhotoViewController.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 22/01/16.
//  Copyright © 2016 keepcoding.io. All rights reserved.
//

@import UIKit;
@class KCGNote;

@interface KCGPhotoViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

-(id) initWithModel:(KCGNote*) note;

@end
