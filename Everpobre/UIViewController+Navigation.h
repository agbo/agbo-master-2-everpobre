//
//  UIViewController+Navigation.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 18/01/16.
//  Copyright © 2016 keepcoding.io. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Navigation)

-(UINavigationController *) wrappedInNavigation;

@end
