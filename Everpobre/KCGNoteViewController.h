//
//  KCGNoteViewController.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 21/01/16.
//  Copyright © 2016 keepcoding.io. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KCGNote;

@interface KCGNoteViewController : UIViewController

-(id) initWithModel: (KCGNote*) model;

@end
