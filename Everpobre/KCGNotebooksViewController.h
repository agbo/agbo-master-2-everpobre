//
//  KCGNotebooksViewController.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 18/01/16.
//  Copyright © 2016 keepcoding.io. All rights reserved.
//

#import "AGTCoreDataTableViewController.h"

@interface KCGNotebooksViewController : AGTCoreDataTableViewController

@end
